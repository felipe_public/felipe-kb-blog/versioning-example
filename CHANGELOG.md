# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [2.0.0](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/compare/v1.1.0...v2.0.0) (2021-02-14)


### ⚠ BREAKING CHANGES

* This new folder is breaking the api.

### Features

* new folder in structure ([b3670f6](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/commit/b3670f6e74ed2ee1e79bd8ed923d34c96f3c05bb))


### CI

* fix changes parser for release notes ([#5](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/issues/5))([@felipefoz](https://gitlab.com/felipefoz)) ([7957559](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/commit/7957559bd231c196c717481d8a6f418aa597b0ff))

## [1.1.0](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/compare/v1.0.0...v1.1.0) (2021-02-14)


### Features

* adds folder structure ([d08ee5a](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/commit/d08ee5a7d26cce485e391a774dad02d088039519)), closes [#2](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/issues/2)
* adds gitignore file ([e3119b0](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/commit/e3119b031d857b18ac545de4d9ac3b6a772974f7)), closes [#3](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/issues/3)
* adds license file ([31d0c2a](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/commit/31d0c2ab5b609ea3987c84804138fff87c525fb4)), closes [#4](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/issues/4)


### CI

* include ci configuration file ([#1](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/issues/1))([@felipefoz](https://gitlab.com/felipefoz)) ([04b203e](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/commit/04b203eebbc0e1a6191e41b415253943430de721))

## 1.0.0 (2021-02-14)


### CI

* adds versioning configuration file ([07cb86c](https://gitlab.com/felipe_public/felipe-kb-blog/versioning-example/commit/07cb86c02db235b5abb66778c3c2304c926f5a31))
